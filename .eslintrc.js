module.exports = {
  env: {
    es6: true,
    browser: true,
    node: true
  },
  settings: {
    "import/resolver": {
      node: {
        extensions: [".js", ".css", ".jsx", ".json"],
        paths: ["./src/", "./"]
      }
    },
    "import/extensions": [".js", ".jsx", ".css"],
    "import/ignore": [
      "node_modules",
      "build",
      "\\.(coffee|scss|less|hbs|svg|json)$"
    ],
    react: {
      pragma: "React",
      version: "16.4"
    },
    propWrapperFunctions: ["forbidExtraProps", "exact", "Object.freeze"],
    flowtype: {
      onlyFilesWithFlowAnnotation: true
    }
  },
  parser: "babel-eslint",
  parserOptions: {
    sourceType: "module"
  },
  extends: ["plugin:prettier/recommended", "plugin:flowtype/recommended"],
  plugins: ["react", "prettier", "flowtype"],
  rules: {
    "prettier/prettier": "error",
    "flowtype/no-types-missing-file-annotation": 1,
    "no-unused-expressions": [
      "error",
      {
        allowShortCircuit: false,
        allowTernary: false,
        allowTaggedTemplates: false
      }
    ],
    "react/jsx-uses-react": ["error"],
    "react/jsx-uses-vars": "error",
    "react/jsx-no-bind": [
      "warn",
      {
        ignoreRefs: true,
        allowArrowFunctions: false,
        allowBind: true
      }
    ]
  }
};
