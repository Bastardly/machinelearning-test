// @flow

/*
Description

This code does not work yet. These are not the droids you are looking for, carry on...

*/

import React from "react";
import moment from "moment";
import trainModel from "./trainModel";

const nodes = [
  {
    value: null
  }
];

const buttons = [1, 2, 3, 4, 5, 6, 7, 8, 9];

// newValues er et array af mockValues
class Index extends React.PureComponent {
  state = {
    counter: 0,
    first: null,
    second: null,
    modelData: []
  };
  // probability = nodes.reduce((tempProb, node) => {
  //   const average = findAverageValue(node.values);
  // }, 1);

  // man skal kunne indsende nodes, dvs. tilføje til node collection.
  // man skal kunne indsende fix points

  handleClick = ({ target }) => {
    const { counter, modelData, first, second } = this.state;
    const newCounter = counter + 1;
    const value = parseInt(target.value, 10);
    if (newCounter === 1) {
      this.setState({
        first: { id: "first", value },
        counter: newCounter
      });
    }
    if (newCounter === 2) {
      this.setState({
        second: { id: "second", value },
        counter: newCounter
      });
    }
    if (newCounter === 3) {
      this.setState({
        counter: 0,
        modelData: [].concat(...modelData, [
          {
            first,
            second,
            third: { value, date: moment() }
          }
        ])
      });
    }
    console.log(this.state);
  };

  mockNode = {
    name: "string",
    id: 0,
    x: 1,
    value: 0
  };

  makeCollection = (first, second, date) => ({
    date,
    nodes: [
      {
        ...this.mockNode,
        name: "first",
        id: first.id,
        value: parseInt(first.value, 10)
      },
      {
        ...this.mockNode,
        name: "second",
        id: second.id,
        value: parseInt(second.value, 10)
      }
    ]
  });

  trainModel = () => {
    const { modelData } = this.state;
    const modelScraps = modelData.map(({ first, second, third }) => ({
      collection: this.makeCollection(first, second, third.date),
      fixPoint: third
    }));
    const fixPoints = modelScraps.map(d => d.fixPoint);
    const nodeCollections = modelScraps.map(d => d.collection);
    const test = trainModel(nodeCollections, fixPoints, 1);
    console.log("Result:", test);
  };

  render() {
    return (
      <div>
        {buttons.map(button => {
          return (
            <button
              type="button"
              onClick={this.handleClick}
              value={button}
              key={`button${button}`}
            >
              {button}
            </button>
          );
        })}
        <div>
          <button
            type="button"
            onClick={this.trainModel}
            disabled={this.state.counter !== 0}
          >
            TRAIN MODEL
          </button>
        </div>
      </div>
    );
  }
}

export default Index;
