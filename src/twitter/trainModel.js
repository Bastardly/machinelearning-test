// @flow
import moment from "moment";
import mapNodes from "./map-nodes";
import { getAverageValue, getAllValues } from "./helpers";

import type { NodeCollection, ModelFixPoint } from "_types";

// Dette er den som laver modellen ud fra alle de data vi har. ****************************************************************
const trainModel = (
  nodeCollections: NodeCollection[],
  fixPoints: ModelFixPoint[],
  days: number
) => {
  const findData = date => {
    const afterDate = moment(date).subtract(days, "days");
    return nodeCollections.filter(collection => {
      const valDate = moment(collection.date);
      return valDate.isBefore(date) && valDate.isAfter(afterDate);
    });
  };

  type Data = {
    collections: NodeCollection[],
    ...ModelFixPoint
  };

  // Find data fra alle fixPoints, fx 7 dage tilbage fra datoen.
  const data: Data[] = fixPoints.map(({ value, date }) => ({
    date,
    value,
    collections: findData(date) // nodeData er den collections som falder inden for tidsrammen
  }));
  const averageFixpointValue = getAverageValue(getAllValues(fixPoints));

  // find de nodes som stikker ud og map dem of find x
  const mappedNodes = mapNodes(data, nodeCollections, averageFixpointValue);

  // returner model.
  return mappedNodes;
};

export default trainModel;
