// @flow;
import Moment from "moment";

export type Node = {
  name: string,
  id: string,
  x: number,
  value: number
};
export type NewValue = {
  id: number,
  value: number
};

// skal opsamle data x antal dage tilbage fra denne dato.
export type ModelFixPoint = {
  value: number, // var det en succes eller ej. +/-
  date: Moment
};

//
export type NodeCollection = {
  nodes: Node[],
  date: Moment
};
