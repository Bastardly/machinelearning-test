// @flow
import { getSum, getAverageValue, calculateX, getDifference } from "./helpers";

const getValueLists = nodeCollections => {
  const allNodeTypeIds = [];
  nodeCollections.forEach(collection => {
    collection.nodes.forEach(node => {
      if (allNodeTypeIds.every(id => id !== node.id)) {
        allNodeTypeIds.push(node.id);
      }
    });
  });

  const valueLists = allNodeTypeIds.map(id => {
    const values = [];
    nodeCollections.forEach(collection => {
      collection.nodes.forEach(node => {
        if (node.id === id) {
          values.push(node.value);
        }
      });
    });
    const sum = getSum(values);
    return {
      id,
      values,
      sum,
      averageValue: getAverageValue(sum)
    };
  });
  return valueLists;
};

// Find nodeweight på hver node
// data = data ud fra en bestemt tidsperiode, som hver har en tilhørende værdi.
// nodeCollections = alle data samlinger.
const mapNodes = (data, nodeCollections, averageFixpointValue) => {
  const valueLists = getValueLists(nodeCollections); // Baseret på alle værdier.
  const diffMap = []; // Som vi fordeler dataværdierne ud fra.
  data.forEach(({ value, collections }) => {
    const diffs = [];
    // Vi tager værdien fra hver node, og pusher diff til diffs, for at finde den totale afvigelse for den givne periode.
    // Vi ved at for hver value, er der en række tilhørende collections, som værdien skal tildeles. Nu skal hver node
    // have tildelt værdi baseret på afvigelse.
    collections.forEach(collection => {
      collection.nodes.forEach(node => {
        const nodeTypeValues = valueLists.find(
          nodeType => nodeType.id === node.id
        );
        const difference = getDifference(
          node.value,
          nodeTypeValues.averageValue
        );
        const differenceProcentage =
          (difference / nodeTypeValues.averageValue).toFixed(2) * 100; // @todo eller node.value - lidt i tvivl
        diffs.push({
          nodeId: node.id,
          difference,
          differenceProcentage // vises som hele tal
        });
      });
    });
    // const diffValues = diffs.map(
    //   ({ differenceProcentage }) => differenceProcentage
    // );
    // @todo er usikker på om jeg skal bruge average... er jo med i andre beregninger.
    // const averageDiffProcentage = getAverageValue(diffValues); // Dvs et samlet billede for de givne periode.
    const totalDiffProcentage = diffs.reduce((a, b) => {
      if (b.differenceProcentage < 0) {
        b.differenceProcentage = b.differenceProcentage * -1;
      }
      return a + b.differenceProcentage;
    }, 0);
    console.log("total %", totalDiffProcentage);

    const nodeWeight =
      ((100 / totalDiffProcentage) * value) / averageFixpointValue;

    const nodes = diffs.map(({ nodeId, differenceProcentage }) => ({
      nodeId,
      x: calculateX(nodeWeight, differenceProcentage)
    }));

    diffMap.push({
      nodes,
      value
    });
  });
  // Nu er diffMap fyldt, så nu kan vi mappe indflydelsen (x) på hver node. Sweeet!
  const xCollection = {};
  const nodeIdCollection = [];
  diffMap.forEach(({ nodes }) => {
    nodes.forEach(({ x, nodeId }) => {
      console.log(x);
      if (!xCollection[nodeId]) {
        xCollection[nodeId] = [];
      }
      xCollection[nodeId].push(x);
      nodeIdCollection.push(nodeId);
    });
  });
  return nodeIdCollection.map(id => {
    const col = xCollection[id];
    const sum = getSum(col);
    return {
      id,
      x: (sum / col).toFixed(2) // Heu presto, x er fundet og returneret med x!!
    };
  });
};

export default mapNodes;
