export const getDifference = (val, average) => {
  if (!average) return 1;
  return (average - val) / average;
};

export const nodeCalc = (diff, x) => {
  return 1 + Math.pow(diff, x);
};

export const calculatePropabilityWithKnownValues = nodes => {
  // Calculate with new data from known vnodeCollectionsalues
};

export const getSum = numbers => {
  if (!numbers) return 0;
  if (typeof numbers === "number") return numbers;
  return numbers.reduce((a, b) => a + b, 0);
};

export const getAverageValue = numbers => {
  if (!numbers) return 0;
  const average = getSum(numbers) / parseInt(numbers.length || 1, 10);
  return average;
};

export const getAllValues = arr =>
  arr.map(({ value }) => value).filter(Boolean);

// x er aldrig negativ, men er den potens som vægter differencens indflydelse.
export const calculateX = (nodeWeight, diff) => {
  let x = 1; // dvs den gør ikke noget.
  console.log("diff", diff);

  // Er lidt usikker på om diff of nodeWeight skal byttes om...
  let counter = 0;
  // while (Math.pow(nodeWeight, 1 / x) - diff > 1.1 && counter < 100) {
  if (diff && diff > 1) {
    while (Math.pow(diff, x) < nodeWeight && counter < 8000) {
      x = x + 0.1;
      counter++;
    }
  }
  console.log(
    counter,
    "diff",
    diff,
    "---",
    "weight",
    nodeWeight,
    "x",
    x,
    "calc",
    Math.pow(diff, x)
  );
  return x;
};
