function pattern(values, limit) {
  const length = (values && values.length) || 0;
  if (length) {
    const patterns = {};
    let checkLength = 2;

    while ((checkLength < length && !limit) || (limit && checkLength < limit)) {
      let innerStart = 0;
      while (innerStart + checkLength < length) {
        const valueString = values
          .slice(innerStart, parseInt(innerStart + checkLength + 1, 10))
          .toString();
        if (!patterns[valueString]) {
          patterns[valueString] = 1;
        } else {
          patterns[valueString] = parseInt(patterns[valueString], 10) + 1;
        }
        innerStart++;
      }
      checkLength++;
    }
    return patterns;
  }
}

export default pattern;
