// @flow
// The more hits, the more popular
// With a test with five buttons, where the benchmark was 20% - This test alone scored 30%
// That's a 50% increase!

type Node = {
  id: number,
  name: string
};

type MostPopular = {
  id: number,
  allocatedProcentage: number,
  numberOfHits: number
};

type NodeCollection = {
  nodes: Node[],
  mostPopular: MostPopular[],
  numberOfPasses: number
};

function getMostPopular(
  valueId: number,
  nodeCollection: NodeCollection,
  numberOfPasses: number
) {
  let { mostPopular, nodes } = nodeCollection;
  mostPopular = (mostPopular.length && mostPopular) || nodes;
  mostPopular = mostPopular.map(node => {
    const hits = node.numberOfHits || 0;
    const numberOfHits = node.id === valueId ? hits + 1 : hits || 0;
    return {
      ...node,
      numberOfHits,
      allocatedProcentage: parseInt(
        (numberOfHits / numberOfPasses) * 100,
        10
      ).toFixed(2)
    };
  });
  return [...mostPopular];
}

export default getMostPopular;
