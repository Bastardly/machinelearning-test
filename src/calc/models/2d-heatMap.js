// @flow
// This is a 2d heatmap. This will calculate the based on the previous selection.

// YCount er et array som representerer y-aksen på heatmappet.
type HeatMapElement = {
  id: number,
  YCount: number[]
};

function getHeatmap(value, previousValue, nodeCollection) {
  // do magics
  if (isNaN(previousValue)) {
    // første run - fyld heatmap op med nodes
    return nodeCollection.nodes;
  }
  previousValue = parseInt(previousValue, 10);
  const { heatMap } = nodeCollection;
  return heatMap.map(
    (element): HeatMapElement => {
      if (previousValue !== element.id) {
        return element;
      }
      const YCount = element.YCount || [];
      const YCountValue = YCount[value] || 0;
      YCount[value] = YCountValue + 1;
      return {
        ...element,
        YCount
      };
    }
  );
}

export default getHeatmap;
