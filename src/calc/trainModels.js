// @flow
import getMostPopular from "./models/most-popular";
import getHeatmap from "./models/2d-heatMap";
import getPrediction from "./prediction/";

type NodeCollection = {
  numberOfPasses: number
};

const trainModel = (
  value: number,
  previousValue?: number,
  nodeCollection: NodeCollection,
  allValues: number[],
  mostCorrectModels,
  correct
) => {
  const numberOfPasses = parseInt(nodeCollection.numberOfPasses, 10) + 1;
  const updatedCollection = {
    ...nodeCollection,
    mostPopular: getMostPopular(value, nodeCollection, numberOfPasses),
    heatMap: getHeatmap(value, previousValue, nodeCollection),
    numberOfPasses
  };
  return getPrediction(
    updatedCollection,
    value,
    allValues,
    mostCorrectModels,
    correct
  );
};

export default trainModel;
