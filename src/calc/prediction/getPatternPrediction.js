import { getAllValues } from "../helpers";

function getPatternPrediction(
  patterns,
  allValues,
  currentValue,
  nodes,
  allocatedProcentage
) {
  // do magics!
  if (allValues.length < 10) return void 0;
  const length = allValues.length;
  const fiveBase = [].concat(
    ...allValues.slice(length - 3, length),
    currentValue
  );

  const myAlgoritm = (threeStr, fourStr, fiveStr) => {
    const getValue = str => parseInt(patterns[str], 10) || 0;
    const threeVal = getValue(threeStr);
    const fourVal = getValue(fourStr);
    const fiveVal = getValue(fiveStr);
    return Math.pow(threeVal, 3) + Math.pow(fourVal, 4) + Math.pow(fiveVal, 5);
  };

  const predictions = nodes.map(({ id }) => {
    const five = [].concat(...fiveBase, id);
    const fiveStr = five.toString();
    const fourStr = five.slice(1, 5).toString();
    const threeStr = five.slice(2, 5).toString();
    return {
      value: myAlgoritm(threeStr, fourStr, fiveStr),
      id
    };
  });
  const totalNumberOfPoints = getAllValues(predictions);
  return predictions.map(node => ({
    allocatedProcentage:
      (node.value / totalNumberOfPoints) * allocatedProcentage,
    id: node.id
  }));

  // lags arrays af fx 5 eller 3 i længde, og sammenlign med patterns ud fra hver node id.
  // find de højeste værdier, men tag hensyn til længde
  // returner bedste kandidater med allocatedProcentage ... som de andre
}

export default getPatternPrediction;
