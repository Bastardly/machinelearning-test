// @flow
import type { HeatMapElement } from "../models/2d-heatMap";
import { getSum } from "../helpers";
import getPatternPrediction from "./getPatternPrediction";
import getBestGuesses from "./getBestGuesses";

const allocatedProcentage = 100;

function getHeatMapPrediction(
  heatMap: HeatMapElement,
  value,
  allocatedProcentage
) {
  const { YCount } = heatMap[value];
  if (!YCount) return heatMap;
  const totalNumberOfHits = getSum(YCount);

  return heatMap.map((node, index) => {
    const hits = YCount[index] || 0;
    return {
      ...node,
      allocatedProcentage: (hits / totalNumberOfHits) * allocatedProcentage
    };
  });
}

function getMostPopularPrediction(mostPopular, allocatedProcentage) {
  return mostPopular.map(node => ({
    ...node,
    allocatedProcentage: (
      (node.allocatedProcentage * allocatedProcentage) /
      100
    ).toFixed(1)
  }));
}

function handleProcentage(node) {
  if (!node || (node && !node.allocatedProcentage)) return 0;
  return parseInt(node.allocatedProcentage, 10).toFixed(1);
}
function getAllocatedProcentage(mostCorrectModels, correct, name) {
  if (!mostCorrectModels || !mostCorrectModels.length) return 10;
  const model = mostCorrectModels.find(model => model.id === name);
  if (!model) return 5;
  const numberOfTimesCorrect = model && model.numberOfTimesCorrect; // to get started
  if (!numberOfTimesCorrect) return 5;
  console.log(
    name,
    (numberOfTimesCorrect / correct) * allocatedProcentage,
    "%"
  );
  return (numberOfTimesCorrect / correct) * allocatedProcentage;
}

function getPrediction(
  nodeCollection,
  value: number,
  allValues: number[],
  mostCorrectModels,
  correct
) {
  // Pt. er der ingen brug for procentage
  const { mostPopular, heatMap, nodes, patterns } = nodeCollection;
  const patternPrediction;
  if (patterns) {
    patternPrediction = getPatternPrediction(
      patterns,
      allValues,
      value,
      nodeCollection.nodes,
      getAllocatedProcentage(mostCorrectModels, correct, "patternPrediction")
    );
    // Færdiggør prediction...
  }
  const heatMapPrediction = getHeatMapPrediction(
    heatMap,
    value,
    getAllocatedProcentage(mostCorrectModels, correct, "heatMapPrediction")
  );

  const mostPopularPrediction = getMostPopularPrediction(
    mostPopular,
    getAllocatedProcentage(mostCorrectModels, correct, "mostPopularPrediction")
  );

  const guessCollection = {
    patternPrediction,
    heatMapPrediction,
    mostPopularPrediction
  };
  console.log("col", guessCollection);

  const bestGuesses = getBestGuesses(guessCollection);

  const prediction = nodes.reduce(
    (bestNode, { id }) => {
      const procentage = parseInt( // brug getSum
        handleProcentage(heatMapPrediction[id]) +
          handleProcentage(patternPrediction && patternPrediction[id]) +
          handleProcentage(mostPopularPrediction[id]),
        10
      );
      if (procentage > bestNode.procentage) {
        return { id, procentage };
      }
      return bestNode;
    },
    { id: null, procentage: 0 }
  ) ;
  return {
    ...nodeCollection,
    prediction: prediction.id,
    bestGuesses
  };
}

export default getPrediction;
