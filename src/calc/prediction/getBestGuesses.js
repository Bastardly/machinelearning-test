function findBestGuess(guesses) {
  if (!guesses || !guesses.length || !guesses[0].allocatedProcentage) return;
  const bestGuess = guesses.sort(
    (a, b) => b.allocatedProcentage - a.allocatedProcentage
  )[0];
  return bestGuess.id;
}

function getBestGuesses(guesses) {
  Object.keys(guesses).forEach(key => {
    const guess = guesses[key];
    if (!guess) {
      debugger;
      return;
    }
    guesses[key].bestGuessId = findBestGuess(guess);
  });
  return guesses;
}

export default getBestGuesses;
