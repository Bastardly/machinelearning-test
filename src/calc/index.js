// @flow

import React from "react";
import trainModels from "./trainModels";
import generatePattern from "./models/generate-pattern";

// newValues er et array af mockValues
class Index extends React.PureComponent {
  state = {
    allValues: [],
    bestGuesses: [],
    prediction: null,
    feedbackText: "Click the buttons, and I will guess",
    correct: 0,
    mostCorrectModels: [
      {
        id: "patternPrediction",
        numberOfTimesCorrect: 0
      },
      {
        id: "heatMapPrediction",
        numberOfTimesCorrect: 0
      },
      {
        id: "mostPopularPrediction",
        numberOfTimesCorrect: 0
      }
    ],
    nodeCollection: {
      numberOfPasses: 0,
      mostPopular: [],
      prediction: null,
      nodes: [
        {
          id: 0,
          name: "Gandalf"
        },
        {
          id: 1,
          name: "Frodo"
        },
        {
          id: 2,
          name: "Gollum"
        },
        {
          id: 3,
          name: "Sauron"
        },
        {
          id: 4,
          name: "Aragorn"
        }
      ]
    }
  };

  getFeedbackText = (currentId, nextPrediction) => {
    const {
      nodeCollection: { numberOfPasses, nodes },
      prediction
    } = this.state;
    this.setState({
      prediction: nextPrediction
    });
    if (numberOfPasses <= 10) return "Still learning...";
    if (currentId === prediction) {
      this.setState({
        correct: parseInt(this.state.correct, 10) + 1
      });
      return `I guessed correct. ${
        nodes.find(node => node.id === currentId).name
      }`;
    }

    return `Crap, I guessed ${nodes.find(node => node.id === prediction).name}`;
  };

  getMostCorrectModels = (mostCorrectModels, bestGuesses, currentId) =>
    mostCorrectModels.map(({ id, numberOfTimesCorrect = 0 }) => {
      const bestGuessObj = bestGuesses[id];
      if (!bestGuessObj || !bestGuessObj.bestGuessId) {
        return { id, numberOfTimesCorrect };
      }

      const bestGuess = bestGuessObj.bestGuessId;
      return {
        id,
        numberOfTimesCorrect:
          bestGuess === currentId
            ? numberOfTimesCorrect + 1
            : numberOfTimesCorrect
      };
    });

  trainModel = currentId => {
    const {
      nodeCollection,
      nodeCollection: { numberOfPasses },
      allValues,
      mostCorrectModels,
      correct
    } = this.state;
    // NAN bliver brugt i validering om heatmap skal køres.
    const previousValue = parseInt(
      numberOfPasses > 0 && allValues[numberOfPasses - 1],
      10
    );
    const updatedCollection = trainModels(
      currentId,
      previousValue,
      nodeCollection,
      allValues,
      mostCorrectModels,
      correct
    );
    const bestGuesses = updatedCollection.bestGuesses;
    this.setState({
      allValues: [].concat(allValues, currentId),
      feedbackText: this.getFeedbackText(
        currentId,
        updatedCollection.prediction
      ),
      nodeCollection: updatedCollection,
      bestGuesses,
      mostCorrectModels: this.getMostCorrectModels(
        mostCorrectModels,
        bestGuesses,
        currentId
      )
    });
  };

  handleClick = ({ target }) => {
    const id = parseInt(target.value, 10);
    this.trainModel(id);
  };

  runRandom100 = () => this.runRandom(100);

  runRandom1000 = () => this.runRandom(1000);

  runRandom = passes => {
    const numberOfNodes = this.state.nodeCollection.nodes.length;
    let x = 0;
    let {
      prediction,
      previousValue,
      bestGuesses,
      nodeCollection,
      nodeCollection: { numberOfPasses },
      allValues = [],
      correct,
      mostCorrectModels
    } = this.state;
    while (x < passes) {
      x++;
      const currentId = Math.floor(Math.random() * Math.floor(numberOfNodes));
      previousValue = parseInt(
        numberOfPasses > 0 && allValues[numberOfPasses - 1],
        10
      );
      const updatedCollection = trainModels(
        currentId,
        previousValue,
        nodeCollection,
        allValues,
        mostCorrectModels,
        correct
      );
      if (currentId === prediction && numberOfPasses > 10) {
        correct = parseInt(correct, 10) + 1;
      }
      // const UpdatedMostCorrectModels = () =>
      //   mostCorrectModels.map(({ id, numberOfTimesCorrect = 0 }) => {
      //     const bestGuessObj = updatedCollection.bestGuesses[id];
      //     if (!bestGuessObj || !bestGuessObj.bestGuessId) {
      //       return { id, numberOfTimesCorrect };
      //     }
      //
      //     const bestGuess = bestGuessObj.bestGuessId;
      //     return {
      //       id,
      //       numberOfTimesCorrect:
      //         bestGuess === currentId
      //           ? numberOfTimesCorrect + 1
      //           : numberOfTimesCorrect
      //     };
      //   });
      // debugger;
      allValues = [].concat(...allValues, currentId);
      nodeCollection = updatedCollection;
      prediction = updatedCollection.prediction;
      numberOfPasses = updatedCollection.numberOfPasses;
      bestGuesses = updatedCollection.bestGuesses;
      mostCorrectModels = this.getMostCorrectModels(
        mostCorrectModels,
        bestGuesses,
        currentId
      );
    }
    this.setState({
      nodeCollection,
      correct,
      allValues
    });
  };

  generatePattern = () => {
    const value = parseInt(this.inputRef.current.value, 10);
    const limit = value.isNan ? 5 : value;
    const patterns = generatePattern(this.state.allValues, limit);
    this.setState({
      nodeCollection: {
        ...this.state.nodeCollection,
        patterns,
        correct: 0,
        allValues: []
      }
    });
  };

  inputRef = React.createRef();

  render() {
    const { feedbackText, nodeCollection, correct, allValues } = this.state;
    return (
      <div>
        <h3>{feedbackText}</h3>
        {nodeCollection.nodes.map(node => {
          return (
            <button
              type="button"
              onClick={this.handleClick}
              value={node.id}
              key={`node${node.id}`}
            >
              {node.name}
            </button>
          );
        })}
        <div>
          <h3>
            {nodeCollection.numberOfPasses > 10 &&
              `Correct guess ${(correct /
                (nodeCollection.numberOfPasses - 10)) *
                100} % `}
          </h3>
          <div style={{ maxWidth: "80%" }}>
            {allValues.map(value => value + " ")}
          </div>
          <button type="button" onClick={this.runRandom100}>
            Run and guess 100 randomly
          </button>
          <button type="button" onClick={this.runRandom1000}>
            Run and guess 1000 randomly
          </button>
          <button type="button" onClick={this.generatePattern}>
            GeneratePattern
          </button>
          <div style={{ padding: "20px" }}>
            <input
              type="number"
              style={{ width: "100%", height: "48px", borderRadius: "5px" }}
              ref={this.inputRef}
              placeholder="Insert length limit, default 5"
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Index;
